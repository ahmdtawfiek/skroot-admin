import Vue from "vue";
import Router from "vue-router";
import Layout from "@/Layout";
import cookie from "js-cookie";

Vue.use(Router);

// Base Routes
export const constantRouterMap = [
  {
    path: "/",
    component: Layout,
    redirect: "/home",
    children: [
      {
        path: "/home",
        component: () => import("@/views/home"),
        name: "home",
        meta: {
          icon: "mdi-view-dashboard",
          title: "dashboard"
        }
      }
    ]
  },
  {
    path: "/home",
    component: Layout,
    redirect: "/home",
    children: [
      {
        path: "/home",
        component: () => import("@/views/home"),
        name: "Home",
        meta: {
          icon: "mdi-view-dashboard",
          title: "home"
        }
      }
    ]
  },
  {
    path: "/dashboard",
    component: Layout,
    redirect: "/dashboard",
    children: [
      {
        path: "/dashboard",
        component: () => import("@/views/dashboard"),
        name: "dashboard",
        meta: {
          icon: "mdi-view-dashboard",
          title: "dashboard"
        }
      }
    ]
  },
  {
    path: "/notifications",
    component: Layout,
    redirect: "/notifications",
    children: [
      {
        path: "/notifications",
        component: () => import("@/views/notifications"),
        name: "notifications",
        meta: {
          icon: "mdi-view-notifications",
          title: "notifications"
        }
      }
    ]
  },
  {
    path: "/refund",
    component: Layout,
    redirect: "/refund",
    children: [
      {
        path: "/refund",
        component: () => import("@/views/refund"),
        name: "refund",
        meta: {
          icon: "mdi-view-refund",
          title: "refund"
        }
      }
    ]
  },
  {
    path: "/suppliers",
    component: Layout,
    redirect: "/suppliers",
    children: [
      {
        path: "/suppliers",
        component: () => import("@/views/suppliers"),
        name: "suppliers",
        meta: {
          title: "suppliers"
        }
      },
      {
        path: "/suppliers/:id",
        component: () => import("@/views/suppliers/crud"),
        name: "supplierEdit",
        meta: {
          title: "supplierEdit"
        }
      },
      {
        path: "/suppliers/add",
        component: () => import("@/views/suppliers/crud"),
        name: "supplierAdd",
        meta: {
          title: "supplierAdd"
        }
      }
    ]
  },
  {
    path: "/orders",
    component: Layout,
    redirect: "/orders",
    children: [
      {
        path: "/orders",
        component: () => import("@/views/orders"),
        name: "orders",
        meta: {
          icon: "mdi-view-dashboard",
          title: "orders"
        }
      },
      {
        path: "/orders/:id",
        component: () => import("@/views/orders/details"),
        name: "orderDetails",
        meta: {
          icon: "mdi-view-dashboard",
          title: "orderDetails"
        }
      }
    ]
  },
  {
    path: "/profile",
    component: Layout,
    redirect: "/profile",
    children: [
      {
        path: "/profile",
        component: () => import("@/views/profile"),
        name: "profile",
        meta: {
          icon: "mdi-view-dashboard",
          title: "profile"
        }
      }
    ]
  },
  {
    path: "/support",
    component: Layout,
    redirect: "/support",
    children: [
      {
        path: "/support",
        component: () => import("@/views/support"),
        name: "support",
        meta: {
          icon: "mdi-view-dashboard",
          title: "support"
        }
      },
      {
        path: "/support/:id",
        component: () => import("@/views/support/details"),
        name: "chatDetails",
        meta: {
          icon: "mdi-view-dashboard",
          title: "chatDetails"
        }
      }
    ]
  },
  {
    path: "/settings",
    component: Layout,
    redirect: "/settings",
    children: [
      {
        path: "/settings",
        component: () => import("@/views/settings"),
        name: "settings",
        meta: {
          icon: "mdi-view-dashboard",
          title: "settings"
        }
      },
      {
        path: "/settings/configs",
        component: () => import("@/views/settings/configs"),
        name: "settingsConfigs",
        meta: {
          icon: "mdi-view-dashboard",
          title: "configs"
        }
      }
    ]
  },
  {
    path: "/current-requests",
    component: Layout,
    redirect: "/current-requests",
    children: [
      {
        path: "/current-requests",
        component: () => import("@/views/current-requests"),
        name: "currentRequests",
        meta: {
          icon: "mdi-view-dashboard",
          title: "requests"
        }
      },
      {
        path: "/current-requests/:id",
        component: () => import("@/views/current-requests/details"),
        name: "currentRequestDetails",
        meta: {
          icon: "mdi-view-dashboard",
          title: "RequestDetails"
        }
      }
    ]
  },
  {
    path: "/login",
    component: () => import("@/views/login/index"),
    hidden: true
  },
  {
    path: "/401",
    component: () => import("@/views/errorPage/401"),
    name: "Page401",
    hidden: true
  },
  {
    path: "/404",
    component: () => import("@/views/errorPage/404"),
    name: "Page404",
    hidden: true
  },
  { path: "*", redirect: "/404", hidden: true }
];

const router = new Router({
  mode: "history",
  //   base: process.env.BASE_URL,
  routes: constantRouterMap
});
router.beforeEach((to, from, next) => {
  if (cookie.get("token")) {
    if (to.path === "/login") {
      next("/");
    } else {
      next();
    }
  } else {
    if (to.path === "/login") {
      next();
    } else {
      next("/login");
    }
  }
});

export default router;
