import Layout from "@/Layout";
export const asyncRouterMap = [
  {
    path: "/home",
    component: Layout,
    redirect: "/home/list",
    meta: {
      title: "home",
      icon: "mdi-account-circle"
    },
    children: [
      {
        path: "list",
        component: () => import("@/views/home/index"),
        name: "homeList",
        // hidden: true,
        meta: {
          title: "home",
          icon: "mdi-account-circle"
        }
      }
    ]
  },
  { path: "*", redirect: "/404", hidden: true }
];
