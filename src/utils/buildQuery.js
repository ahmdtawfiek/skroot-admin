const convertQuery = query => {
  const queryString = Object.keys(query)
    .map(key => {
      if (query[key] === null) {
        return key;
      } else {
        return key + "=" + query[key];
      }
    })
    .join("&");
  return queryString;
};
export default convertQuery;
