import request from "@/utils/request";
import cookies from "js-cookie";
// import checkThereIsQuery from "@/helpers/checkQuery";
const localeCookie = cookies.get("language");

import convertQuery from "@/utils/buildQuery";

export function index(reqName, query = {}, locale = localeCookie) {
  return request({
    url: `${reqName}?a&${convertQuery(query)}`,
    headers: { "X-locale": locale ? locale : cookies.get("language") },
    method: "get"
  });
}
export function indexQuery(reqName, query = {}, locale = localeCookie) {
  return request({
    url: `${reqName}${convertQuery(query)}`,
    headers: { "X-locale": locale ? locale : cookies.get("language") },
    method: "get"
  });
}

export function showData(reqName, slug, locale = localeCookie) {
  return request({
    url: `${reqName}/${slug}`,
    headers: { "X-locale": locale },
    method: "get"
  });
}

export function storeData(reqName, data) {
  return request({
    url: `${reqName}`,
    method: "post",
    data
  });
}
export const updateData = (reqName, data, id) => {
  return request({
    url: `${reqName}/${id}`,
    method: "post",
    // headers: { "X-locale": locale ? locale : cookies.get("language") },
    data
  });
};
export const updateDataId = (reqName, data) => {
  return request({
    url: `${reqName}`,
    method: "post",
    // headers: { "X-locale": locale ? locale : cookies.get("language") },
    data
  });
};
export function deleteData(reqName, id) {
  return request({
    url: `${reqName}/${id}`,
    method: "delete"
  });
}
export function mediaUpdate(id, data) {
  return request({
    url: `media/${id}`,
    method: "post",
    data
  });
}
