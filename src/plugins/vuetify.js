import Vue from "vue";
import Vuetify from "vuetify/lib";
import ChangeDirection from "@/helpers/ChangeDirection";

Vue.use(Vuetify);

export default new Vuetify({
  rtl: ChangeDirection(),
  icons: {
    iconfont: "mdi"
  },
  theme: {
    themes: {
      light: {
        accent: "#82B1F2",
        primary: "#9543d6",
        secondary: "#424242",
        error: "#FF5252",
        info: "#f9c118",
        success: "#2fb68f",
        warning: "#FFC107"
      }
    }
  }
});
